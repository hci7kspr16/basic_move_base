/*********************************************************************
*
* Software License Agreement (BSD License)
*
*  Copyright (c) 2008, Willow Garage, Inc.
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*
* Author: Eitan Marder-Eppstein
*         Mike Phillips (put the planner in its own thread)
*********************************************************************/
#include <move_base/move_base.h>
#include <cmath>

#include <boost/algorithm/string.hpp>
#include <boost/thread.hpp>

#include <geometry_msgs/Twist.h>

namespace move_base {

  MoveBase::MoveBase(tf::TransformListener& tf) :
    tf_(tf),
    as_(NULL),
    controller_frequency_(10)
    {

    as_ = new MoveBaseActionServer(ros::NodeHandle(), "move_base", boost::bind(&MoveBase::executeCb, this, _1), false);

    ros::NodeHandle private_nh("~");
    ros::NodeHandle nh;

    //for commanding the base
    vel_pub_ = nh.advertise<geometry_msgs::Twist>("cmd_vel", 1);
    current_goal_pub_ = nh.advertise<geometry_msgs::PoseStamped>("current_goal", 0 );

    ros::NodeHandle action_nh("move_base");
    action_goal_pub_ = action_nh.advertise<move_base_msgs::MoveBaseActionGoal>("goal", 1);

    //we'll provide a mechanism for some people to send goals as PoseStamped messages over a topic
    //they won't get any useful information back about its status, but this is useful for tools
    //like nav_view and rviz
    
    goal_sub_ = nh.subscribe<geometry_msgs::PoseStamped>("simple_goal", 1, boost::bind(&MoveBase::goalCB, this, _1));
    odom_sub_ = nh.subscribe<nav_msgs::Odometry>("odom", 1, boost::bind(&MoveBase::odomCB, this, _1));


    //we're all set up now so we can start the action server
    as_->start();

  }
  
  void MoveBase::odomCB(const nav_msgs::Odometry::ConstPtr& odom)
  {
    robotOdom_ = odom;
  }
  
  void MoveBase::goalCB(const geometry_msgs::PoseStamped::ConstPtr& goal){
    ROS_DEBUG_NAMED("move_base","In ROS goal callback, wrapping the PoseStamped in the action message and re-sending to the server.");
    move_base_msgs::MoveBaseActionGoal action_goal;
    action_goal.header.stamp = ros::Time::now();
    action_goal.goal.target_pose = *goal;

    action_goal_pub_.publish(action_goal);
  }

  MoveBase::~MoveBase(){
    publishZeroVelocity();
    if(as_ != NULL)
      delete as_;
  }

  void MoveBase::publishZeroVelocity(){
    geometry_msgs::Twist cmd_vel;
    cmd_vel.linear.x = 0.0;
    cmd_vel.linear.y = 0.0;
    cmd_vel.angular.z = 0.0;
    vel_pub_.publish(cmd_vel);
  }

  bool MoveBase::isQuaternionValid(const geometry_msgs::Quaternion& q){
    //first we need to check if the quaternion has nan's or infs
    if(!std::isfinite(q.x) || !std::isfinite(q.y) || !std::isfinite(q.z) || !std::isfinite(q.w)){
      ROS_ERROR("Quaternion has nans or infs... discarding as a navigation goal");
      return false;
    }

    tf::Quaternion tf_q(q.x, q.y, q.z, q.w);

    //next, we need to check if the length of the quaternion is close to zero
    if(tf_q.length2() < 1e-6){
      ROS_ERROR("Quaternion has length close to zero... discarding as navigation goal");
      return false;
    }

    //next, we'll normalize the quaternion and check that it transforms the vertical vector correctly
    tf_q.normalize();

    tf::Vector3 up(0, 0, 1);

    double dot = up.dot(up.rotate(tf_q.getAxis(), tf_q.getAngle()));

    if(fabs(dot - 1) > 1e-3){
      ROS_ERROR("Quaternion is invalid... for navigation the z-axis of the quaternion must be close to vertical.");
      return false;
    }

    return true;
  }

  void MoveBase::executeCb(const move_base_msgs::MoveBaseGoalConstPtr& move_base_goal)
  {
    if(!isQuaternionValid(move_base_goal->target_pose.pose.orientation)){
      as_->setAborted(move_base_msgs::MoveBaseResult(), "Aborting on goal because it was sent with an invalid quaternion");
      return;
    }

    geometry_msgs::PoseStamped goal = goalToGlobalFrame(move_base_goal->target_pose);
    current_goal_pub_.publish(goal);
    current_goal_ = goal;
    
    ros::Rate r(controller_frequency_);

    ros::NodeHandle n;
    while(n.ok())
    {
      if(as_->isPreemptRequested()){
        if(as_->isNewGoalAvailable()){
          //if we're active and a new goal is available, we'll accept it, but we won't shut anything down
          move_base_msgs::MoveBaseGoal new_goal = *as_->acceptNewGoal();

          if(!isQuaternionValid(new_goal.target_pose.pose.orientation)){
            as_->setAborted(move_base_msgs::MoveBaseResult(), "Aborting on goal because it was sent with an invalid quaternion");
            return;
          }

          goal = goalToGlobalFrame(new_goal.target_pose);

          state_ = CONTROLLING;


          //publish the goal point to the visualizer
          ROS_DEBUG_NAMED("move_base","move_base has received a goal of x: %.2f, y: %.2f", goal.pose.position.x, goal.pose.position.y);
          current_goal_pub_.publish(goal);
	  current_goal_ = goal;

        }
        else {
          //if we've been preempted explicitly we need to shut things down
          resetState();

          //notify the ActionServer that we've successfully preempted
          ROS_DEBUG_NAMED("move_base","Move base preempting the current goal");
          as_->setPreempted();

          //we'll actually return from execute after preempting
          return;
        }
      }

      //for timing that gives real time even in simulation
      ros::WallTime start = ros::WallTime::now();

      //the real work on pursuing a goal is done here
      bool done = executeCycle();

      //if we're done, then we'll return from execute
      if(done)
        return;

      //check if execution of the goal has completed in some way

      ros::WallDuration t_diff = ros::WallTime::now() - start;
      ROS_DEBUG_NAMED("move_base","Full control cycle time: %.9f\n", t_diff.toSec());

      r.sleep();
      //make sure to sleep for the remainder of our cycle time
      if(r.cycleTime() > ros::Duration(1 / controller_frequency_) && state_ == CONTROLLING)
        ROS_WARN("Control loop missed its desired rate of %.4fHz... the loop actually took %.4f seconds", controller_frequency_, r.cycleTime().toSec());
    }

    //if the node is killed then we'll abort and return
    as_->setAborted(move_base_msgs::MoveBaseResult(), "Aborting on the goal because the node has been killed");
    return;
  }

  double MoveBase::distance(const geometry_msgs::Pose& p1, const geometry_msgs::Pose& p2)
  {
    return hypot(p1.position.x - p2.position.x, p1.position.y - p2.position.y);
  }

  bool MoveBase::executeCycle(){

    //we need to be able to publish velocity commands
    geometry_msgs::Twist cmd_vel;
    
    current_position_ = robotOdom_->pose.pose;
    //push the feedback out
    geometry_msgs::PoseStamped current_ps;
    current_ps.pose = current_position_;
    current_ps.header.stamp = ros::Time::now();
    move_base_msgs::MoveBaseFeedback feedback;
    feedback.base_position = current_ps;
    as_->publishFeedback(feedback);

    //check to see if we've reached our goal
    if(isGoalReached())
      {
	ROS_DEBUG_NAMED("basic_move_base","Goal reached!");
	resetState();
	as_->setSucceeded(move_base_msgs::MoveBaseResult(), "Goal reached.");
	return true;
      }

    if(computeVelocityCommands(cmd_vel))
      {
	ROS_DEBUG_NAMED( "move_base", "Got a valid command from the local planner: %.3lf, %.3lf, %.3lf",
			 cmd_vel.linear.x, cmd_vel.linear.y, cmd_vel.angular.z );
	//last_valid_control_ = ros::Time::now();
	//make sure that we send the velocity command to the base
	vel_pub_.publish(cmd_vel);
      }

    //we aren't done yet
    return false;
  }
  
  void MoveBase::resetState(){

    publishZeroVelocity();

  }

  bool MoveBase::isGoalReached()
  {
    //Check the distance between the goal and the current position within some epsilon
    double reachedEps = 0.1;
    if (distance(current_position_, current_goal_.pose) < reachedEps)
      {
	return true;
      }
    else
      return false;
  }
  
  bool MoveBase::computeVelocityCommands(geometry_msgs::Twist &cmd_vel)
  {
    //Split a command into two components: angular and linear
    //First spin to point towards the target if necessary
    //Close the loop around the z component (planar angle)
    double angleP = 0.5;
    double angleMaxVel = 0.1;
    double angleEps = 0.3; //epsilon for heading being close 'enough' to start moving linearly
    double linearVel = 1.0;
    
    ROS_DEBUG("Current x,y: %1.2f, %1.2f", current_position_.position.x, current_position_.position.y);
    ROS_DEBUG("Goal x,y: %1.2f, %1.2f", current_goal_.pose.position.x, current_goal_.pose.position.y);
    
    double desiredHeading = atan2(current_goal_.pose.position.y - current_position_.position.y,
				  current_goal_.pose.position.x - current_position_.position.x);

    //atan2: -pi to pi
    double roll, pitch, yaw;
    tf::Quaternion q_heading(current_position_.orientation.x,
			   current_position_.orientation.y,
			   current_position_.orientation.z,
			   current_position_.orientation.w);
    tf::Matrix3x3(q_heading).getRPY(roll, pitch, yaw);
    
    ROS_DEBUG("Current heading: %1.2f", yaw);
    ROS_DEBUG("Desired heading: %1.2f", desiredHeading);
    double nativeAngleError = desiredHeading - yaw;
    double equivAngleError = desiredHeading - yaw + 2*M_PI;

    double angleError;
    if (fabs(nativeAngleError) < fabs(equivAngleError))
      {
	angleError = nativeAngleError;
      }
    else
      {
	angleError = equivAngleError;
      }
      
    if (fabs(angleError) < angleEps)
      {
	//Headed in the right direction towards the goal
	cmd_vel.linear.x = linearVel;
      }

    //Move in the shortest direction
    if (fabs(angleError) > M_PI)
      cmd_vel.angular.z = -angleP * angleError;
    else
       cmd_vel.angular.z = angleP * angleError;  

         
    ROS_INFO("Computed error of %1.2f, issuing command %1.2f", angleError, cmd_vel.angular.z);
    //cmd_vel.angular.z = 0;
    return true;
  }

  geometry_msgs::PoseStamped MoveBase::goalToGlobalFrame(const geometry_msgs::PoseStamped& goal_pose_msg)
  {
    return goal_pose_msg;
  }
};
